using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Este codigo es por ejemplo para tirar aros a botellas teniendo en cuenta la posicion del mouse.
public class TirarAros : MonoBehaviour
{
    [SerializeField] float _InitialVelocity;
    [SerializeField] float _Angle;
   // [SerializeField] LineRenderer _Line;
    [SerializeField] float _Step;
  

    [SerializeField] Transform _FirePoint;
    private Camera _cam;
    private void Start()
    {
        _cam = Camera.main;

    }

    private void Update()
    {
        
        float angle = _Angle*Mathf.Deg2Rad;
        Vector3 targetPos= _cam.ScreenToWorldPoint(Input.mousePosition);
        float v0;
        float time;
        CalculatePath(targetPos, angle, out  v0, out time);

       /* DrawPath(v0, angle, _Step);*/

        if(Input.GetKeyDown(KeyCode.Space))
        {
           
            
            StopAllCoroutines();
            StartCoroutine(Coroutine_Movement(v0, angle));

        }

    }
   /* private void DrawPath(float v0, float angle, float step)
    {
        
        step = Mathf.Max(0,01f,step);
        float totalTime = 0.1f;
        _Line.positionCount = (int)(totalTime/ step) +2;
        int count = 0;
        for (float i = 0; i < totalTime; i += step)
        {
            
            float z =  v0* i*Mathf.Cos(angle);
            float x = v0* i *Mathf.Cos(angle);
            float y =  v0* i *Mathf.Sin(angle)-0.5f * -Physics.gravity.y * Mathf.Pow(i, 2);
           // _Line.SetPosition(count,  new Vector3(x , y, z));
            count++;
        }
        float zfinal =  v0*totalTime *Mathf.Cos(angle);
        float xfinal =  v0*totalTime *Mathf.Cos(angle);
        float yfinal = v0*totalTime *Mathf.Sin(angle)-0.5f * -Physics.gravity.y * Mathf.Pow(totalTime, 2);
        //_Line.SetPosition(count,  new Vector3(xfinal,yfinal,zfinal));
        

    }*/

    private void CalculatePath(Vector3 targetPos, float angle, out float v0, out float time)
    {
        float xt = targetPos.x;
        float zt = targetPos.z;
    
        float yt= targetPos.y;
        float g= -Physics.gravity.y;
        float v1 = Mathf.Pow(xt, 2) * g;
        float v2 = 2 * xt * Mathf.Sin(angle) * Mathf.Cos(angle);
        float v3 = 2 * yt * Mathf.Pow(Mathf.Cos(angle),2);
        v0 = Mathf.Sqrt(v1 / (v2-v3));
        time = xt / (v0 * Mathf.Cos(angle));


    }
    
    IEnumerator Coroutine_Movement(float v0, float angle)
    {
        
        float t = 0;
        while(t <100)
        {
       
         //float x = x1 + v0 * t * Mathf.Cos(angle);
            float x =   v0 * t * Mathf.Cos(angle);
            float z =  v0 * t * Mathf.Cos(angle);
            float y =  v0 * t * Mathf.Sin(angle ) - (1f/2f) * -Physics.gravity.y * Mathf.Pow(t,2);
            transform.position = new Vector3(0, y,z);
            transform.position = _FirePoint.position +  new Vector3(0, y,z);
            t+= Time.deltaTime;
            yield return null;
        }


    }
            
            
}
    

