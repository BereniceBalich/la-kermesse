using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class caminoDeLaTirada : MonoBehaviour
{
    LineRenderer lr;
    Rigidbody rb;
    Vector3 startPosition;
    Vector3 startVelocity;

    float InitialForce = 20;

    float InitialAngle = 30;
    Quaternion rot;
    int i = 0;
    int NumberOfPoint = 20;
    float timer=0.1f;

    void Start()
    {
        lr = GetComponent<LineRenderer>();
        rb =GetComponent<Rigidbody>();
        rot =Quaternion.Euler(InitialAngle, 0, 0);
    }
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            drawline();
        }
        if(Input.GetKeyUp(KeyCode.Space))
        {
            rb.AddForce(rot*(InitialForce*transform.forward));
            lr.enabled =false;

        }
    }
    private void drawline()
    {
        i = 0;
        lr.positionCount = NumberOfPoint;
        lr.enabled = true;
        startPosition=transform.position;
        startVelocity = rot*(InitialForce*transform.forward)/rb.mass;
        lr.SetPosition(i, startPosition);
        for(float j = 0; j< lr.positionCount; j+= timer)
        {
            i++;
            Vector3 linePosiition= startPosition + j* startVelocity;
            linePosiition.y= startPosition.y+startVelocity.y*j+0.5f*Physics.gravity.y*j*j;
            lr.SetPosition(i,linePosiition);


        }
    }

}
