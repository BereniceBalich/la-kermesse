using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GarraMecanica : MonoBehaviour
{

    public float rango;
    public LayerMask layerMaskPlayer;
    public bool estaAlerta;
    public Transform positionPlayer;
    public float Speed;
    public float tiempoActual;
    public bool atacar;
    public GameManager gameManager;

      
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(new Vector3(positionPlayer.position.x, transform.position.y, positionPlayer.position.z));
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(positionPlayer.position.x, transform.position.y, positionPlayer.position.z), Speed * Time.deltaTime);

       estaAlerta = Physics.CheckSphere(transform.position, rango, layerMaskPlayer); 

        if(estaAlerta == true)
        {
            atacar = false;
            tiempoActual -= Time.deltaTime;

            if (tiempoActual <= 0)
            {
                atacar = true;
            }

            if(atacar == true)
            {
               gameManager.Ataque();

            }




        }
    }



}
