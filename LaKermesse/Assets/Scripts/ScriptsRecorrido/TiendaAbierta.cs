using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiendaAbierta : MonoBehaviour
{

    public float rango;
    public LayerMask layerMaskPlayer;
    public bool mirando;
    public Transform positionEntrada;
    public GameManager gameManager;
    public CamaraController camara;
     public float Speed;


    void Start()
    {
        
    }

   
    void Update()
    {
       mirando  = Physics.CheckSphere(transform.position, rango, layerMaskPlayer);

        if(mirando == true)
        {
            Debug.Log("entrando");
            camara.transform.LookAt(new Vector3(positionEntrada.position.x, positionEntrada.position.y, positionEntrada.position.z));
            camara.transform.position = Vector3.MoveTowards(camara.transform.position, new Vector3(1.154f, 0.6190001f, 2.684f), Speed * Time.deltaTime);
        }


    }
}
