using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;

public class GameManager : MonoBehaviour
{

    public GameObject Inicio;
    public GameObject jugador;

   // public string archivoDeGuardado;
  

    public DatosJuego datosJuego = new DatosJuego();


    public CamaraController camara;
    public float tiempo;

    private void Awake()
    {
        //archivoDeGuardado = Application.dataPath + "/datosJuego.json";

        jugador = GameObject.FindGameObjectWithTag("Player");
        Inicio = GameObject.FindGameObjectWithTag("Inicio");
    }

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;

        CargarDatos();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            CargarDatos();
        }
        if (Input.GetKeyDown(KeyCode.G))
        {
            //GuardarDatos();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            Reiniciar();
        }

    }


   public void Ataque()
   {
        Debug.Log("Garra Mecanica atac�");
      
        camara.Muerte();
        tiempo -= Time.deltaTime;
        if (tiempo<= 0)
        {
            Cursor.lockState = CursorLockMode.None;
         
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            Reiniciar();

        }
    
   }

    public void EntrarMinijuego()
    {
      

       
             Cursor.lockState = CursorLockMode.None;
      
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 2);
        

       
    }

    public void Final()
    {
        Cursor.lockState = CursorLockMode.None;
        Reiniciar();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 2);
    }

    private void CargarDatos()
    {
       /* if (File.Exists(archivoDeGuardado))
        {
            string contenido = File.ReadAllText(archivoDeGuardado);
            datosJuego = JsonUtility.FromJson<DatosJuego>(contenido);

            Debug.Log("Posicion Jugador : " + datosJuego.posicion);
           jugador.transform.position = datosJuego.posicion;
        }
        else
        {
            Debug.Log("El archivo no existe");
        }*/
    }

  /*public void GuardarDatos()
  {

        DatosJuego nuevosDatos = new DatosJuego()
        {
            posicion = jugador.transform.position
        };

        string cadenaJSON = JsonUtility.ToJson(nuevosDatos);

        File.WriteAllText(archivoDeGuardado, cadenaJSON);

        

        Debug.Log("Archivo Guardado");

  }*/

    public void Reiniciar()
    {

        DatosJuego nuevosDatos = new DatosJuego()
        {
           posInicial = Inicio.transform.position
        };

        //string cadenaJSON = JsonUtility.ToJson(nuevosDatos);

        //File.WriteAllText(archivoDeGuardado, cadenaJSON);



    

    }
}
