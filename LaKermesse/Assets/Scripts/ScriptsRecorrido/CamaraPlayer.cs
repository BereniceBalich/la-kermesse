using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraPlayer : MonoBehaviour
{
    public Vector2 sensibility;
  
    public float rango;
    public LayerMask layerMaskEntrada;
    public LayerMask layerMaskFinal;
    public bool mirando;
   
    public bool final;

  

    public GameManager gameManager;


    void Start()
    {
        mirando = false;
      
    }


    void Update()
    {

     
            float horizontal = Input.GetAxis("Mouse X");

         if(horizontal != 0)
         {
            transform.Rotate(Vector3.up * horizontal * sensibility.x);
            Debug.Log("moviendo camara");
        }
        

      
        

        mirando = Physics.CheckSphere(transform.position, rango, layerMaskEntrada);

        final = Physics.CheckSphere(transform.position, rango, layerMaskFinal);

        if (mirando == true)
        {
            
          gameManager.EntrarMinijuego();
                  
                
            
          
          
        }
        else
        {
            //gameManager.GuardarDatos();
        }
        if (final == true)
        {

            gameManager.Final();

        }

    }

   

  
}
