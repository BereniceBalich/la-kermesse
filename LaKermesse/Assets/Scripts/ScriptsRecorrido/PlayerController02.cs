using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController02 : MonoBehaviour
{

    private new Rigidbody rigidbody;


    public float speed;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        
    }
    public void UpdateMovement()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        if(horizontal !=0 || vertical !=0)
        {
            Vector3 motion = (transform.forward * vertical + transform.right * horizontal).normalized * speed;
            rigidbody.velocity = motion;
            Debug.Log("moviendose");
        }
        else
        {
            rigidbody.velocity = Vector3.zero;
        }
    }
    
    void Update()
    {
        UpdateMovement();
    }
}
