using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCamara : MonoBehaviour
{
   public new Transform camera;
    public Vector2 sensibility;
    void Start()
    {
        
        Cursor.lockState = CursorLockMode.Locked;
    }

   
    void Update()
    {
        float horizontal = Input.GetAxis("Mouse X");
        float vertical  = Input.GetAxis("Mouse Y");

        if (horizontal != 0)
        {
            transform.Rotate(Vector3.up * horizontal * sensibility.x);
          
        }

        if(vertical != 0)
        {
            //camera.Rotate(Vector3.left * vertical * sensibility.x);

          float angle = camera.localEulerAngles.x - vertical * sensibility.y;

            camera.localEulerAngles = Vector3.right * angle;


        }
    }
}
