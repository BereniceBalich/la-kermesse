using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlTiempo : MonoBehaviour
{
    public float rango;
    public LayerMask layerMaskGlobo;
    public bool Perder;
    public float tiempoActual;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        

        tiempoActual -= Time.deltaTime;

        if (tiempoActual <= 0)
        {
            Perder = Physics.CheckSphere(transform.position, rango, layerMaskGlobo);

            if(Perder == true)
            {
                Cursor.lockState = CursorLockMode.None;
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1 );
            }
            else
            {
                
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 2);
            }
        }

    }
}
